<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-accept-language library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use InvalidArgumentException;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Stringable;

/**
 * ConnectionClient class file.
 * 
 * This class is an implementation of a client which adds accept-language
 * headers on incoming requests.
 * 
 * @author Anastaszor
 */
class AcceptLanguageClient implements ClientInterface, Stringable
{
	
	/**
	 * The inner client.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_client;
	
	/**
	 * The configuration.
	 * 
	 * @var AcceptLanguageConfiguration
	 */
	protected AcceptLanguageConfiguration $_configuration;
	
	/**
	 * Builds a new DoNotTrackClient with the given inner client.
	 * 
	 * @param ClientInterface $client
	 * @param AcceptLanguageConfiguration $configuration,
	 */
	public function __construct(ClientInterface $client, ?AcceptLanguageConfiguration $configuration = null)
	{
		$this->_client = $client;
		if(null === $configuration)
		{
			$configuration = new AcceptLanguageConfiguration();
		}
		
		$this->_configuration = $configuration;
		if($this->_configuration->getAcceptChain()->isEmpty())
		{
			// apply default mozilla-us configuration
			try
			{
				$this->_configuration->addValue('en-US');
				$this->_configuration->addValue('en', 0.5);
			}
			catch(InvalidArgumentException)
			{
				// ignore, should not happen
			}
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Client\ClientInterface::sendRequest()
	 */
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		if(!$request->hasHeader('Accept-Language') && !$this->_configuration->getAcceptChain()->isEmpty())
		{
			try
			{
				$request = $request->withHeader('Accept-Language', $this->_configuration->getAcceptChain()->getHeaderValue());
			}
			// @codeCoverageIgnoreStart
			catch(InvalidArgumentException $e)
			// @codeCoverageIgnoreEnd
			{
				// nothing to do, should not happen
			}
		}
		
		return $this->_client->sendRequest($request);
	}
	
}
