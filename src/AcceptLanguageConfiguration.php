<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-accept-language library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use InvalidArgumentException;
use PhpExtended\Locale\Locale;
use Stringable;

/**
 * AcceptLanguageConfiguration class file.
 * 
 * This class stores the configuration for the accept-language client.
 * 
 * @author Anastaszor
 */
class AcceptLanguageConfiguration implements Stringable
{
	
	/**
	 * The accept chain.
	 * 
	 * @var ?AcceptLanguageChainInterface
	 */
	protected ?AcceptLanguageChainInterface $_acceptChain = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Adds the given locale with the given q-value to the chain.
	 * 
	 * @param string $localeStr
	 * @param float $qvalue
	 * @return AcceptLanguageConfiguration
	 * @throws InvalidArgumentException
	 */
	public function addValue(string $localeStr, float $qvalue = 1.0) : AcceptLanguageConfiguration
	{
		$this->getAcceptChain()->appendItem(new AcceptLanguageItem(new Locale($localeStr), $qvalue));
		
		return $this;
	}
	
	/**
	 * Gets the accept chain.
	 * 
	 * @return AcceptLanguageChainInterface
	 */
	public function getAcceptChain() : AcceptLanguageChainInterface
	{
		if(null === $this->_acceptChain)
		{
			$this->_acceptChain = new AcceptLanguageChain([]);
		}
		
		return $this->_acceptChain;
	}
	
}
