<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-accept-language library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\AcceptLanguageChain;
use PhpExtended\HttpClient\AcceptLanguageConfiguration;
use PhpExtended\HttpClient\AcceptLanguageItem;
use PhpExtended\Locale\Locale;
use PHPUnit\Framework\TestCase;

/**
 * AcceptLanguageConfigurationTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\AcceptLanguageConfiguration
 *
 * @internal
 *
 * @small
 */
class AcceptLanguageConfigurationTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var AcceptLanguageConfiguration
	 */
	protected AcceptLanguageConfiguration $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetAcceptLanguageAdded() : void
	{
		$expected = new AcceptLanguageChain([
			new AcceptLanguageItem(new Locale('en_US'), 0.7),
		]);
		
		$this->assertEquals($expected, $this->_object->addValue('en_US', 0.7)->getAcceptChain());
	}
	
	public function testGetEmptyAcceptLanguageChain() : void
	{
		$this->assertEquals(new AcceptLanguageChain([]), $this->_object->getAcceptChain());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new AcceptLanguageConfiguration();
	}
	
}
